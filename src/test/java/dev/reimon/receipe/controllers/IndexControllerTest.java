package dev.reimon.receipe.controllers;

import dev.reimon.receipe.repositories.RecipeRespository;
import dev.reimon.receipe.services.RecipeService;
import dev.reimon.receipe.services.RecipeServiceImp;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


public class IndexControllerTest {

    IndexController indexController;

    @Mock
    RecipeServiceImp recipeService;
    @Mock
    Model model;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        indexController = new IndexController(recipeService);
    }

    @Test
    public void getIndexPage() {
        assertEquals(indexController.getIndexPage(model),"index");

        verify(recipeService, times(1)).getRecipes();
        verify(model, times(1)).addAttribute(eq("categories"), anySet());
    }
}