package dev.reimon.receipe.services;

import dev.reimon.receipe.domain.Recipe;
import dev.reimon.receipe.repositories.RecipeRespository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RecipeServiceImpTest {

    RecipeServiceImp recipeService;

    @Mock
    RecipeRespository recipeRespository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        recipeService = new RecipeServiceImp(recipeRespository);
    }

    @Test
    public void getRecipes() throws Exception {

        Recipe recipe = new Recipe();
        HashSet recipeHashSet = new HashSet();
        recipeHashSet.add(recipe);

        //le dice a mockito lo que ha de devolver cuando se llame el metodo getRecipes()
        when(recipeService.getRecipes()).thenReturn(recipeHashSet);

        Set<Recipe> recipes = recipeService.getRecipes();


        assertEquals(recipes.size(), 1);

        //Verifica que se invoque x veces el metodo findAll cuando se llama
        verify(recipeRespository, times(1)).findAll();
    }

}