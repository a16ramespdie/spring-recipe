package dev.reimon.receipe.domain;

public enum Difficulty {
    EASY, MODERATE, HARD
}
