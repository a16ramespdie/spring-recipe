package dev.reimon.receipe.controllers;

import dev.reimon.receipe.domain.Category;
import dev.reimon.receipe.repositories.CategoryRespository;
import dev.reimon.receipe.repositories.UnitOfMesureRepository;
import dev.reimon.receipe.services.RecipeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
public class IndexController
{
    private RecipeService recipeService;


    public IndexController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @RequestMapping({"", "/", "/index"})
    public String getIndexPage(Model model) {

        model.addAttribute("categories", recipeService.getRecipes());

        return "index";
    }
}
