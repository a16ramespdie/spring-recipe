package dev.reimon.receipe.repositories;

import dev.reimon.receipe.domain.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CategoryRespository extends CrudRepository<Category, Long> {

    //findAllByDescription devuelve todos los resultados
    //findByDescription devuelve un resultado
    Optional<Category> findByDescription(String description);
}
