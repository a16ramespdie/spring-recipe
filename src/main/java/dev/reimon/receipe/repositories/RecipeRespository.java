package dev.reimon.receipe.repositories;

import dev.reimon.receipe.domain.Recipe;
import org.springframework.data.repository.CrudRepository;

public interface RecipeRespository extends CrudRepository<Recipe, Long> {
}
