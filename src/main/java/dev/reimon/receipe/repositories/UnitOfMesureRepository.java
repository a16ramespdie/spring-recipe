package dev.reimon.receipe.repositories;

import dev.reimon.receipe.domain.UnitOfMesure;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UnitOfMesureRepository extends CrudRepository<UnitOfMesure, Long> {

    Optional<UnitOfMesure> findByDescription(String Description);
}
