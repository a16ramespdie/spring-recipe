package dev.reimon.receipe.services;

import dev.reimon.receipe.domain.Recipe;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface RecipeService {
    public Set<Recipe> getRecipes();
}
