package dev.reimon.receipe.services;

import dev.reimon.receipe.domain.Recipe;
import dev.reimon.receipe.repositories.RecipeRespository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
public class RecipeServiceImp implements RecipeService {

    private RecipeRespository recipeRespository;

    public RecipeServiceImp(RecipeRespository recipeRespository) {
        this.recipeRespository = recipeRespository;
    }

    @Override
    public Set<Recipe> getRecipes() {
        log.debug("Im' in the service");

        Set<Recipe> recipeSet = new HashSet<>();
        recipeRespository.findAll().iterator().forEachRemaining(recipeSet::add);
        return recipeSet;
    }
}
